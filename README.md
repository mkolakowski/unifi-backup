# Unifi Backup

**Make sure you have SSH setup on your UDM**

https://help.ui.com/hc/en-us/articles/360049612874-UniFi-UDM-How-to-Login-to-the-Dream-Machine-using-SSH

**Also ensure Auto backups are enabled on your UDM/UDM Pro**

1. SSH in and install the below dependencies 
- Unzip
- Nano
```
Apt install unzip
Apt install nano
```
2. Install rclone
```
curl https://rclone.org/install.sh | sudo bash
```
3. setup rclone to point to cloud storage: 
```
Rclone config
```
**make sure you name it gdrive**

https://www.howtogeek.com/451262/how-to-use-rclone-to-back-up-to-google-drive-on-linux/

4. Open and edit Crontab
- Choose Nano as the text editor (or one you are familar with)
- add the below to the end of the crontab, assuming you named your storage gdriveand your unifi backs up at midnight
```
Crontab -e
```
note, the below is a single line
```
10 * * * * rclone copy -P --include autobackup_$(dpkg -l | awk '$2=="unifi" { print substr ($3,0, ( match ( $3,"-" ) ) -0 ) } ' )*.unf /data/unifi/data/backup/autobackup/ gdrive:/Controller/$(dpkg -l | awk '$2=="unifi" { print substr ($3,0, ( match ( $3,"-" ) ) -0 ) } ' )
```